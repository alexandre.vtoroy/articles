
# Why Do Cats Knead?

Cat kneading, also known as 'making biscuits' is an adorable behaviour, usually seen when they're having a good fuss!
You may be wondering why they do this and want to discover the reasoning behind the behaviour,
which is why we've put together this handy guide to tell you everything you need to know.

Cat kneading is a common cat behaviour, often displayed as the rhythmic pushing of their paws in and out at alternating times.
Many people think that it looks like they’re kneading dough, which is why it’s also adorably known as
‘making biscuits’.
Cats love to do this on soft surfaces like cosy blankets, squishy cushions, or most commonly your lap when they’re having a good fuss!

If you have a cat that does this, you may be wondering why do cats knead? Which is why we’ve put together this article
so you can find out the truth behind your feline friend’s baking habits.


## What is cat kneading?

Most cats will knead, but not all will do it in the same way. Where the motion of their paws usually remains the same,
the accompanying actions may differ. Some will purr really loudly, where others may do it quietly, others may do it with
just their front paws, whereas some will put their all into it and use all four. Most cats will at least display
the classic making biscuits action. Additionally, whilst cats knead it’s not unusual for them to appear
to be in a trance-like state and look almost glazed over. This just means that they’re completely relaxed.

## Why do cats knead?

There are actually many reasons why cats knead, but the main one is that it’s an instinctive trait that
they inherit from kitten-hood. When kittens feed from their mother they will paw at their mothers’ tummies
to encourage milk flow from their teats.

This is why sometimes cats may dribble while doing it or might suckle at a soft object such as a blanket or cushion,
because they’re expecting the milk that used to come with the movement.

While it may seem unusual that cat kneading is still present as they get older, according to the Blue Cross
it’s actually a huge compliment for you, because when they do this it means that they feel happy, safe and comforted with you,
much like how they felt with their mother!

## Making a nest

Another theory for cats kneading is that it’s a cat behaviour that’s been passed down from their wild ancestors.
Wild cats will paw at piles of leaves or tall grass to create a nest for themselves and their young to relax and sleep in.
By doing this to the ground they’re not only creating a soft nest – similarly to how we fluff pillows – but they’re also
checking for predators, prey or dangerous things hidden in the foliage. So, when your domestic cat does this to your lap
it may be an ingrained habit from their wild history!

## Marking territory

A possible answer to why do cats knead is that they’re trying to mark their territory, because there are scent glands
that release pheromones in their paws. By pushing their paws in and out they activate these scent glands, so they
could be doing this on your lap to mark you as their own and warning other cats to back off.

## Going into heat

Female cats may also knead when they go into heat (also known as oestrus). PetMD says that when they do this it demonstrates
to male cats that they’re ready to mate. They may display other behaviours along with this such as being overly vocal,
displaying more affection than usual and begging to go outside.

Neutering or spaying may decrease these behaviours if they are related to oestrus. And of course, neutering can help
avoid unwanted pregnancies as well as prevent some diseases. For more information, take a look at our cat neutering FAQs.

## What to do if your cat kneading hurts you

Some cats might knead with their claws out which can sometimes feel as though they’re using your lap as a pin cushion!

## Owner holding his cat's paw

It’s important that you never punish them for doing it as it’s an instinctive behaviour and they’re only returning
the affection they feel from you, but there are a few things you can try to decrease the risk of scratches to your body.

If your cat is sinking their claws in a little too much, try putting a soft barrier between you both like a cushion or a blanket.
You can also encourage your cat to lie down and relax by stroking them and gently pushing them down onto your lap.
Alternatively, distract your cat with a fun toy and they’ll soon stop sinking their claws into your lap.

Hopefully that’s answered that burning question, why do cats knead, and now you understand that you should take it
as the ultimate compliment your cat could give you.
