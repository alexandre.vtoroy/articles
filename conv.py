
import os
import pandoc_custom
import xml.dom.minidom

inputFile = "cats1.md"

outputFileFB2 = inputFile.replace(".md", ".fb2")
outputFileEPUB = inputFile.replace(".md", ".epub")
print(f"Converting {inputFile} to {outputFileFB2}, {outputFileEPUB}...")


with open(inputFile, 'r') as f:
    input = f.read()
    lines = input.split("\n")
    for line in lines:
        if line.strip():
            title = line.replace("#", "").strip()
            print(title)
            break


cmd = f'/usr/bin/pandoc --from=markdown --to=fb2 --metadata=title:"{title}" {inputFile} > {outputFileFB2}'
os.system(cmd)

with open(outputFileFB2, "r") as f:
    fb2Book = f.read()
xmlDoc = xml.dom.minidom.parseString(fb2Book)
fb2BookPretty = xmlDoc.toprettyxml(encoding="utf-8").decode("utf-8")
with open(outputFileFB2, "w") as f:
    f.write(fb2BookPretty)

cmd = f'/usr/bin/pandoc --from=markdown --to=epub --metadata=title:"{title}" {inputFile} > {outputFileEPUB}'
os.system(cmd)
